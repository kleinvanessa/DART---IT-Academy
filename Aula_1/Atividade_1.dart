/*  IT Academy - DART

Atividade 1: Explore os recursos que podem ser utilizados de uma variável do tipo String.

Author: Vanessa Lopes Klein
Date: 25/08/2020

*/

import 'dart:io';

vowels_and_consoant(var word){
  
  print("Sua palavra é: $word");
  var count_c=0, count_v=0, count_m =1,many_check, letter;
  print("tamanho da palavra: ${word.length}");
  
  for(int i=0;i<word.length;i++){
    if(word[i] == 'a'||word[i] == 'e'||word[i] == 'i'||word[i] == 'o'||word[i] == 'u'){
      count_v++;
    } else{
      count_c++;
    }
    print("Letra ${word[i]} aparece ${word[i].allMatches(word).length} vez(es)");
  }
  print("Quantidade de consoantes é $count_c e de vogais é $count_v");
  
}

void main(){

  
  print("Digite uma palavra: ");
  String word = stdin.readLineSync();
  
  vowels_and_consoant(word);
  
}