/*  IT Academy - DART

Atividade 2: Crie um programa que avalia quais caracteres são números e quais não são.

Author: Vanessa Lopes Klein
Date: 25/08/2020

*/

import 'dart:io';

find_number(var param){
  for(int i=0;i<param.length;i++){
    int val = int.tryParse(param[i]);
    if(val !=null){
      print("- caracter ${param[i]} é um número.");
    }else{
      print("- caracter ${param[i]} NÃO é um número.");
    }
    
  }
    
}
void main(){

  print("Insira uma string: ");
  String input = stdin.readLineSync();
  print("Sua string é: $input");
  find_number(input);
  
}