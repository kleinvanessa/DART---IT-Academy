/*  IT Academy - DART

Atividade 3: A partir da entrada de um valor calcule:
◦
O Somatório de zero até o número apresentado
◦
O produtório do número apresesentado
◦
O fatorial do número apresentado
◦
Os números primos de 1 até o valor informado
◦
Os números da sequencia de Fibonacci, sendo este número definido pelo valor de entrada. Se a entrada for 5, calcule os cinco primeiros números da sequencia.
---
Author: Vanessa Lopes Klein
Date: 25/08/2020

*/
import 'dart:io';

somatorio(var number){
  var somatorio=0;
  for(int i=0; i<=number; i++){
    somatorio = somatorio + i;
  }
  print("- Somatorio de 0 a $number : $somatorio");
}

fatorial_produtorio(var number){
  var fatorial=1;
  for(int i=1; i<=number; i++){
    fatorial = fatorial * i;
  }
  print("- Fatorial de $number : $fatorial");
  print("- Produtorio de $number : $fatorial");// fatorial = produtorio
}

primos(var number){//encontra quantidade de numeros primos de 1 até o numero fornecido
  var count_p = 2, count=0;
  if(number>2) { 
    for(int i=3; i<=number; i++){
      count =0;
      for(int j=2;j<i; j++){
        if((i%j )==0) count++;
      }
      if(count==0){
        count_p++;
      }      
    }
  }
  else if(number==1){
   count_p=1;
  }
  print("- Existem $count_p numeros primos");
}

fibonacci(var n){
  if (n < 2) {
    return n;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

sequencia_fibonacci(var number){
  stdout.write("- Sequencia Fibonacci: ");
  for (int i = 0; i < number; i++) {
    stdout.write(" ${fibonacci(i)}, ");
  }
}

void main(){

  print("Digite um numero: ");
  int number = int.parse(stdin.readLineSync());
  print("Seu numero é: $number");
  somatorio(number);
  fatorial_produtorio(number);
  primos(number);
  sequencia_fibonacci(number);
  
  
}